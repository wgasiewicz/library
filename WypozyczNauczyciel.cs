﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class WypozyczNauczyciel : Form
    {
        public WypozyczNauczyciel()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {//wypozycz ksiazka
            string path2 = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\ksiazki.txt";
            string pnk = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\pnk" + textBox2.Text + ".txt";
            List<string> plik = File.ReadAllLines(path2).ToList();
            List<string> pozyczone = new List<string>();
            List<string> plik2 = File.ReadAllLines(pnk).ToList();
            string pozycz = plik[Convert.ToInt32(textBox3.Text)];
            int poz = pozycz.Length - 1;

            if (plik2.Count < 3)
            {
                if (textBox3.Text == "")
                {
                    MessageBox.Show("Podaj poprawną pozycję do wypożyczenia!", "Error");
                }
                else if (pozycz.Substring(pozycz.Length - 1) != "n")
                {
                    pozycz = pozycz.Remove(pozycz.Length - 1, 1) + "n";
                    plik[Convert.ToInt32(textBox3.Text)] = pozycz;
                    pozyczone.Add(pozycz);
                    File.WriteAllLines(path2, plik);
                    File.AppendAllLines(pnk, pozyczone);
                }
                else
                {
                    MessageBox.Show("Podana pozycja jest już wypożyczona!", "Error");
                }
            }
            else
            {
                MessageBox.Show("Posiadasz zbyt dużo nieoddanych pozycji!", "Error");
            }
            textBox2.Clear(); textBox3.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {//wyswietlenie pliku z ksiazkami
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\ksiazki.txt";
            List<string> plik = File.ReadAllLines(path).ToList();
            string pom;
            int size = plik.Count;
            richTextBox2.Clear();
            for (int i = 0; i < size; i++)
            {
                pom = plik[i];
                if (pom.Substring(pom.Length - 1) == "n")
                {
                    richTextBox2.SelectionBackColor = Color.Red;
                    richTextBox2.AppendText("[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine);
                }
                if (pom.Substring(pom.Length - 1) != "n")
                {
                    richTextBox2.SelectionBackColor = Color.Green;
                    richTextBox2.AppendText("[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine);
                }
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {//wyswietlenie pliku z filmami
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\filmy.txt";
            List<string> plik = File.ReadAllLines(path).ToList();
            string pom;
            int size = plik.Count;
            richTextBox1.Clear();
            for (int i = 0; i < size; i++)
            {
                pom = plik[i];
                if ( pom.Substring(pom.Length - 12, 1) == "n"| pom.Substring(pom.Length - 1) == "n")
                {
                    richTextBox1.SelectionBackColor = Color.Red;
                    richTextBox1.AppendText("[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine);
                }
                else
                {
                    richTextBox1.SelectionBackColor = Color.Green;
                    richTextBox1.AppendText("[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine);
                }
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {//wypozycz film
            string path2 = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\filmy.txt";
            string pnf = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\pnf" + textBox1.Text + ".txt";
            List<string> plik = File.ReadAllLines(path2).ToList();
            List<string> pozyczone = new List<string>();
            List<string> plik2 = File.ReadAllLines(pnf).ToList();
            string pozycz = plik[Convert.ToInt32(textBox4.Text)];
            if (plik2.Count < 3)
            {
                if (textBox4.Text == "")
                {
                    MessageBox.Show("Podaj poprawną pozycję do wypożyczenia!", "Error");
                }
                else if (pozycz.Substring(pozycz.Length - 1) != "n" && pozycz.Substring(pozycz.Length - 12, 1) != "n")
                {
                    pozycz = pozycz.Remove(pozycz.Length - 1, 1) + "n";
                    plik[Convert.ToInt32(textBox4.Text)] = pozycz;
                    pozyczone.Add(pozycz);
                    File.WriteAllLines(path2, plik);
                    File.AppendAllLines(pnf, pozyczone);
                }
                else
                {
                    MessageBox.Show("Podana pozycja jest już wypożyczona!", "Error");
                }
            }
            else
            {
                MessageBox.Show("Posiadasz zbyt dużo nieoddanych pozycji!", "Error");
            }
            textBox1.Clear(); textBox4.Clear();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //login
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
