﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Biblioteka
{
    public partial class Nauczyciel : Form
    {
        public Nauczyciel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        { //wypozyczanie
            WypozyczNauczyciel wn = new WypozyczNauczyciel();
            wn.ShowDialog();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            nStanKonta nsk = new nStanKonta();
            nsk.ShowDialog();
            this.Hide();
        }
    }
}
