﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class uStanKonta : Form
    {
        public uStanKonta()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//ksiazki

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//filmy
           
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {//oddaj ksiazke
            string puk = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\puk" + textBox2.Text + ".txt";
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\ksiazki.txt";
            List<string> ksiazki = File.ReadAllLines(path).ToList();
            List<string> puczksiazki = File.ReadAllLines(puk).ToList();
            string oddany;
         
            if ((textBox5.Text == "") || Convert.ToInt32(textBox5.Text) >= puczksiazki.Count)
            {
                MessageBox.Show("Podaj poprawna pozycje do zwrotu!", "Error");
            }
            else
            {
                oddany = puczksiazki[Convert.ToInt32(textBox5.Text)];
                puczksiazki.RemoveAt(Convert.ToInt32(textBox5.Text));
                File.WriteAllLines(puk, puczksiazki);


                for (int i = 0; i < ksiazki.Count; i++)
                {
                    if (ksiazki[i] == oddany)
                    {
                        oddany = oddany.Remove(oddany.Length - 1, 1) + "t";
                        ksiazki.Insert(i, oddany);
                        ksiazki.RemoveAt(i + 1);
                        File.WriteAllLines(path, ksiazki);
                        break;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {//oddaj film
            string puf = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\puf" + textBox2.Text + ".txt";
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\filmy.txt";
            List<string> filmy = File.ReadAllLines(path).ToList();
            List<string> puczfilmy = File.ReadAllLines(puf).ToList();
            string oddany;
       
            if ((textBox4.Text == "") || Convert.ToInt32(textBox4.Text) >= puczfilmy.Count)
            {
                MessageBox.Show("Podaj poprawna pozycje do zwrotu!", "Error");
            }
            else
            {
                oddany = puczfilmy[Convert.ToInt32(textBox4.Text)];
                puczfilmy.RemoveAt(Convert.ToInt32(textBox4.Text));
                File.WriteAllLines(puf, puczfilmy);


                for (int i = 0; i < filmy.Count; i++)
                {
                    if (filmy[i] == oddany)
                    {
                        oddany = oddany.Remove(oddany.Length - 12, 12) + "t";
                        filmy.Insert(i, oddany);
                        filmy.RemoveAt(i + 1);
                        File.WriteAllLines(path, filmy);
                        break;
                    }
                }
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {//nr filmu do oddania

        }

        private void button2_Click(object sender, EventArgs e)
        {//wyswietl filmy
            if (textBox2.Text == "")
            {
                MessageBox.Show("Podaj login!", "Error");
            }
            else
            {
                string puf = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\puf" + textBox2.Text + ".txt";
                List<string> plik = File.ReadAllLines(puf).ToList();
                int size = plik.Count;
                textBox1.Clear();
                for (int i = 0; i < size; i++)
                {
                    textBox1.Text += "[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {//wyswietl ksiazki
            if (textBox2.Text == "")
            {
                MessageBox.Show("Podaj login!", "Error");
            }
            else
            {
                string puk = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\puk" + textBox2.Text + ".txt";
                List<string> plik = File.ReadAllLines(puk).ToList();
                int size = plik.Count;
                textBox3.Clear();
                for (int i = 0; i < size; i++)
                {
                    textBox3.Text += "[" + i.ToString() + "]" + "  " + plik[i] + Environment.NewLine;
                }
            }
        }
    }
}
