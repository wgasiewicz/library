﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class UsunKsiazka : Form
    {
        public UsunKsiazka()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {//usuwanie zadanej pozycji
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\ksiazki.txt";
            List<string> plik = File.ReadAllLines(path).ToList();
            if ((textBox1.Text == "") || Convert.ToInt32(textBox1.Text) >= plik.Count)
            {
                MessageBox.Show("Podaj poprawna pozycje do edycji!", "Error");
            }
            else
            {
                plik.RemoveAt(Convert.ToInt32(textBox1.Text));
                File.WriteAllLines(path, plik);
            }           
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
         
        }

        private void button2_Click(object sender, EventArgs e)
        {//wyswietlanie pliku z ksiazkami
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\ksiazki.txt";
            List<string> plik = File.ReadAllLines(path).ToList();
            int size=plik.Count;
            textBox2.Clear();
            for (int i = 0; i < size; i++)
            {
                textBox2.Text +="["+i.ToString()+"]"+"  "+ plik[i]+ Environment.NewLine+"\n";
                
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
