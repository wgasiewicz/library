﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class Logowanie : Form
    {
        public Logowanie()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string uzytkownik = this.textBox1.Text;
            string haslo = this.textBox2.Text;
            if(sprawdzDane(uzytkownik,haslo))
            {
                MessageBox.Show("Logowanie poprawne", "Zalogowano");
                Administrator f4 = new Administrator();
                f4.ShowDialog();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Niepoprawna nazwa użytkownika lub hasło", "Logowanie zakończone niepowodzeniem");
                return;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
        }
        public bool sprawdzDane(string uzytkownik, string haslo)
        {
            if (uzytkownik == "admin" & haslo == "admin")
                return true;
            else return false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
