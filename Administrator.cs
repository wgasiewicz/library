﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class Administrator : Form
    {
        public Administrator()
        {
            InitializeComponent();
        }

        private void Administrator_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dodawanie dodawanie = new Dodawanie();
            dodawanie.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Usun u = new Usun();
            u.ShowDialog();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Edytuj edytuj = new Edytuj();
            edytuj.ShowDialog();
        }
    }
}
