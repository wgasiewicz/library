﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Biblioteka
{
    public partial class RejestracjaNauczyciel : Form
    {
        public RejestracjaNauczyciel()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string path = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\uzytkownicy\nauczyciele.txt";
            string zapis = textBox1.Text + ";" + textBox2.Text + ";" + textBox3.Text + ";" + textBox4.Text+Environment.NewLine;
            File.AppendAllText(path, zapis);
            MessageBox.Show("Rejestracja poprawna.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            string pnk = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\pnk" + textBox3.Text + ".txt";
            string pnf = @"C:\Users\Wojtek\Documents\Visual Studio 2015\Projects\Biblioteka\Biblioteka\PLIKI\wypozyczone\pnf" + textBox3.Text + ".txt";
            File.Create(pnk); File.Create(pnf);
            this.Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            textBox4.PasswordChar = '*';
        }
    }
}
